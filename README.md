# Python Tkinter Tooltip

This is a simple and basic python class aimed to show a tooltip when hovering a tkinter widget.

**How to use it**

To use the tooltip class in your project just follow these simple steps:
1.  copy the file tooltip.py in your project folder
2.  import tooltip in your python file
3.  create an instance of the tooltip class
4.  set the tooltip
  

Insert this line of code to create a tooltip object

`my_tooltip = tooltip.Tooltip(root)`

When creating a new tooltip object only one parameter is required, the target widget (usually root window)
There are few parameters that can be optionally configured.

```
delay=3000,             # milliseconds to wait before showing the tooltip
bg="yellow",            # tooltip background color
fg="#666666",           # tooltip foreground color
pad=(3, 3),             # padding value (x, y)
xos=20,                 # x offset
yos=-10,                # y offset
size=7                  # font size
```
Set the tooltip calling this method using the following syntax:

`your_widget.set(target_widget, tooltip_text, follow)`

Three parameters can be set:
1. **target_widget.**
   
   Which object will activate the tooltip. It can be a label, a button or any other tkinter widget


2. **tooltip_text.** 
   
   The actual text to be displayed on the tooltip. 
   
   If no tooltip_text is provided the tooltip itself will be shown. This way you can setup complex
tooltips by adding content to `your_widget` using standard tkinter methods.


   

3. **follow.** 
   
   Boolean value True or False. If set to true the tooltip will follow your mouse pointer. Default value is False



**Demo examples**

Inside the file called demo.py you will find examples of simple and advanced usage


**Notes**
* The tooltip automatically changes its position if there's not enough space
