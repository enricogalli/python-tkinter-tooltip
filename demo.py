#!/usr/bin/env python3
import tkinter as tk
from PIL import ImageTk, Image
import tkinter.font as font
from tooltip import Tooltip


# Main Window
root = tk.Tk()
root.title("Tooltip Demo")
root.geometry('1200x900')


# Create a button
button = tk.Button(root, text='Wait over me', cursor='hand2')
button.pack(side='top', pady=50)

# Create a label
label = tk.Label(root, text='You can wait over me too')
label.pack(side='top', pady=50)

# Create a second button
second_button = tk.Button(root, text='SECOND BUTTON', cursor='hand2')
second_button.pack(side='top', pady=50)

# create a second label. Will be used with a more complex tooltip
second_label = tk.Label(root, bg='white', text='A complex tooltip')
second_label.pack(side='top', pady=50, ipadx=10, ipady=10)

# Create a Tooltip instance (hint). It will be used to show a tooltip on
# -> the first button (it will follow mouse movements)
# -> the first label (it will be static. Displayed where it was called)
hint = Tooltip(root)

# set Button Tooltip and display it after some time. Follows mouse movements
hint.set(button, 'Tooltip over a button', True)

# set the Label Tooltip and display it after some time. Static, not following mouse movements
hint.set(label, 'Tooltip over the label, not following')

# Create a second Tooltip instance with custom settings (big_hint)
# It will prepare a different tooltip onject
big_hint = Tooltip(root, delay=100, bg="purple", fg="white", pad=(8, 8), offset=(40, -20), size=12)

# set the Second Button tooltip and display it after some time
big_hint.set(second_button, 'Tooltip over a button', True)

# Create a third Tooltip instance to use with some more complex data
complex_tooltip = Tooltip(root, delay=1000, bg="orange", pad=(5, 5))

# COMPLEX TOOLTIP CONTENT - START
# Let's start from a Frame and add content to it
complex_content = tk.Frame(complex_tooltip, bg='white')
complex_content.pack(padx=20, pady=20)

# add a label
tk.Label(complex_content, bg='white', fg='orange', text='A COMPLEX TOOLTIP',
         font=font.Font(size=18)).pack(side='top', pady=20, padx=20)

# add an image
img = ImageTk.PhotoImage(Image.open('assets/images/cup.png'))
tk.Label(complex_content, image=img, bd=0).pack(side='top', pady=20, padx=40) # noqa

# add another label
tk.Label(complex_content, bg='white', fg='#333333', text='Icon from iconfinder.com',
         font=font.Font(size=8)).pack(side='top', pady=(20, 0), padx=20)

# add another last label
tk.Label(complex_content, bg='white', fg='#666666', text='iconsets thesquid-ink-40-free-flat-icon-pack',
         font=font.Font(size=8)).pack(side='top', pady=(0, 20), padx=20)

# COMPLEX TOOLTIP CONTENT - END

# set the complex tooltip to show up when mouse is over the second_label
complex_tooltip.set(second_label, follow=True)

# Start the Tk App
root.mainloop()
