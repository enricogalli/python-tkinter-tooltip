#!/usr/bin/env python3
import tkinter as tk
from PIL import ImageTk, Image
from tooltip import Tooltip

# tiene traccia delle immagini utilizzate (garbage collection workaround)
images = []
labels = []

# elementi utilizzati inseriti in una lista per evitare
# di creare manualmente ogni singolo elemento
mosaic: list[list[str]] = [
    ["w1-1|Microsoft Word - Documento1", "w1-2", "w1-3|Riduci ad icona", "w1-4|Massimizza", "w1-5|Chiudi"],
    ["w2-1|File", "w2-2|Modifica", "w2-3|Visualizza", "w2-4|Inserisci", "w2-5|Formato", "w2-6|Strumenti",
     "w2-7|Tabella", "w2-8|Finestra", "w2-9|Aiuto/Informazioni", "w2-10"],
    ["w3-1|Nuovo", "w3-2|Apri", "w3-3|Salva", "w3-4|Stampa", "w3-5|Cerca", "w3-6|Conrollo ortografico", "w3-7|Taglia",
     "w3-8|Copia", "w3-9|Incolla", "w3-10|Copia formato", "w3-11|Annulla", "w3-12|Ripristina", "w3-13|Salva con nome",
     "w3-14", "w3-15|Tabelle", "w3-16|Excel", "w3-17|Colonne", "w3-18|Disegno", "w3-19|Grafici",
     "w3-20|Elementi non stampabili", "w3-21|Zoom", "w3-22|Aiuto selettivo", "w3-23"],
    ["w4-1|Stile", "w4-2|Font", "w4-3|Dimensione", "w4-4|Grassetto", "w4-5|Corsivo", "w4-6|Sottolineato",
     "w4-7|Allinea a sinistra", "w4-8|Allinea al centro", "w4-9|Allinea a destra", "w4-10|Giustificato",
     "w4-11|Elenco numerato", "w4-12|Elenco puntato", "w4-13|Riduci margine", "w4-14|Aumenta margine",
     "w4-15|Bordi", "w4-16"],
    ["w5"]
]

# main window
root: tk.Tk = tk.Tk()

# Oggetto tooltip. Ne servirà solo uno, lo stile
# applicato sarà identico su tutti gli elementi
t = Tooltip(root, 100, fg="black", size=9, pad=(10, 3))


# Dimensione root uguale all'immagine usata come sorgente
root.geometry("1084x633+50+50")

# nasconde gli elementi della finestra per dare l'illusione
# di avere realmente una finestra di word 6.0
root.overrideredirect(True)

# Chiudi al click del mouse
root.bind_all("<Button>", lambda e: root.destroy())

# genera l'immagine e gli elementi necessari
for riga in mosaic:

    _frame = tk.Frame(root)
    for col in riga:

        try:
            _img, _txt = col.split("|")
        except ValueError:
            _img = col
            _txt = ""

        img = ImageTk.PhotoImage(Image.open('assets/images/'+_img+'.jpg'))
        _lbl = tk.Label(_frame, image=img, bd=0, cursor="hand2") # noqa
        _lbl.pack(side=tk.LEFT)

        if _txt != "":
            t.set(_lbl, _txt, follow=True)

        # evita garbage
        images.append(img)
        labels.append(_lbl)
    _frame.pack(side=tk.TOP)

# aggiunge un testo all'interno della finestra
_testo: str = ("In questo esempio abbiamo ripreso una schermata da una finestra di un vecchio Word6. "
               "L'immagine è stata ritagliata in tanti pezzi minuscoli dividendo ogni icona in un file separato. "
               "Nell'array chiamato 'mosaic' abbiamo dichiarato tutti i tasselli di questo collage "
               "specificando, se necessario, un testo da visualizzare nel Tooltip\n\n"
               "Con questo approccio abbiamo ricreato l'immagine intera con la differenza che ora ogni singola "
               "parte mostrerà dopo 100ms un tooltip con la descrizione della funzionalità dell'icona stessa. "
               "Cliccando con il mouse in una qualsiasi area della finestra il programma viene terminato.\n\n"
               "Il testo che stai leggendo non fa parte dell'immagine ma è un oggetto Label di tkinter.")

_desc = tk.Label(root, text=_testo, bg="white", fg="#666666", wraplength=900, justify=tk.LEFT)
_desc.place(x=100, y=200)

root.mainloop()
