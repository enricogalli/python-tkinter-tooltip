from tkinter import Frame, Label, font

###########################################
#                                         #
#     TKINTER TOOLTIP BY ENRICO GALLI     #
#                                         #
###########################################


class Tooltip(Frame):
    def __init__(
            self,
            target_window,                          # Tooltip parent (usually root window)
            delay: int = 1500,                      # time (ms) before showing the tooltip
            bg: str = "yellow",                     # tooltip background color
            fg: str = "#666666",                    # tooltip foreground color
            pad: tuple[int, int] = (3, 3),          # padding value (x, y)
            offset: tuple[int, int] = (20, -10),    # x offset value (x, y)
            size: int = 7                           # font size
    ):

        # init superClass tk.Frame
        super().__init__()

        # set variables
        self.target_window = target_window
        self.delay = delay
        self.bg = bg
        self.fg = fg
        self.ipadx = pad[0]
        self.ipady = pad[1]
        self.xos = offset[0]
        self.yos = offset[1]
        self.size = size

        self['bg'] = self.bg
        self.visible = False
        self.content_type = str

        # pending tooltip to be displayed
        self.pending_tooltip: None | str = None

    # set tooltip and schedule show
    def start(self, event, content):

        # create a label to be shown if only text is provided
        if isinstance(content, str):

            self.content_type = str
            content = Label(self, text=content, bg=self.bg, fg=self.fg)
            content['font'] = font.Font(size=self.size)
            content.pack(ipadx=self.ipadx, ipady=self.ipady)

            content_obj = content
        else:
            self.content_type = self
            content_obj = self

        # set time to wait before tooltip is displayed
        self.pending_tooltip = content_obj.after(self.delay, lambda: self.show(self.getXY(event)))

    # show tooltip
    def show(self, xypos):

        if self.pending_tooltip is not None:
            self.visible = True
            xpos = xypos[0]
            ypos = xypos[1]

            # adjust x if tooltip is out of the window area
            if xpos + self.winfo_width() > self.target_window.winfo_width():
                xpos -= ((self.xos * 2) + self.winfo_width())

            # adjust y if tooltip is out of the window area
            if ypos + self.winfo_height() > self.target_window.winfo_height():
                ypos -= ((self.yos * 2) + self.winfo_height())

            self.place(x=xpos, y=ypos)

            # show the tooltip above other objects
            self.tkraise()

    # follow tooltip in case <Motion> bind is set
    def follow(self, event):
        if self.visible:
            self.show(self.getXY(event))

    # hide tooltip
    def stop(self):
        if self.content_type == str:
            for child in self.winfo_children():
                child.destroy()
        self.place_forget()
        self.visible = False
        self.pending_tooltip = None

    # add necessary bindings
    def set(self, target_widget, tooltip_content=None, follow=False):

        target_widget.bind("<Enter>", lambda event: self.start(event, tooltip_content))
        target_widget.bind("<Leave>", lambda event: self.stop())
        target_widget.bind("<Button-1>", lambda event: self.stop())

        if follow:
            target_widget.bind("<Motion>", self.follow)

    # find correct x and y position
    def getXY(self, event):
        xpos = event.x_root - self.target_window.winfo_x() + self.xos
        ypos = event.y_root - self.target_window.winfo_y() + self.yos
        return xpos, ypos
